LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)


LOCAL_CFLAGS := -I. -I..
LOCAL_C_INCLUDES := -I.. -I.

LOCAL_MODULE   := lo


LOCAL_SRC_FILES := address.c send.c message.c server.c method.c \
	server_thread.c blob.c bundle.c timetag.c pattern_match.c


LOCAL_LDLIBS := -llog
include $(BUILD_STATIC_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_MODULE := sndfile2
#LOCAL_STATIC_LIBRARIES := sndfile

#include $(BUILD_SHARED_LIBRARY)

